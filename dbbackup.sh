#!/bin/sh
now="$(date +'%d_%m_%Y_%H_%M_%S')"
filename="`uname -n`_DDL_backup_$now".sql
backupfolder="/home/ubuntu/backups"
fullpathbackupfile="$backupfolder/$filename"
logfile="$backupfolder/"backup_log_"$(date +'%Y_%m')".txt
echo "mysqldump started at $(date +'%d-%m-%Y %H:%M:%S')" >> "$logfile"
mysqldump --defaults-extra-file=/home/ubuntu/.my.cnf --default-character-set=utf8 DDL > "$fullpathbackupfile"
echo "mysqldump finished at $(date +'%d-%m-%Y %H:%M:%S')" >> "$logfile"
chown ubuntu "$fullpathbackupfile"
chown ubuntu "$logfile"
echo "file permission changed" >> "$logfile"
cat "$fullpathbackupfile" | sudo scp -i /root/gcp_backups.pem "$fullpathbackupfile" backups@34.77.155.132:~/"$filename"
find "$backupfolder" -name *DDL_backup_* -mtime +8 -exec rm {} \;
echo "old files deleted" >> "$logfile"
echo "operation finished at $(date +'%d-%m-%Y %H:%M:%S')" >> "$logfile"
echo "*****************" >> "$logfile"
exit 0
