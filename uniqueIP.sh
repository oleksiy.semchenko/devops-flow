#!/bin/bash
touch ~/IP.txt
grep -E -o "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)" /var/log/nginx/test_access.log.1 >~/IP.txt
touch ~/uniq.txt
sort ~/IP.txt | uniq -d >~/uniq.txt
awk 'END{print NR}' ~/uniq.txt >numberuniqueIP.txt
echo "Unique IP's total count is:"
cat numberuniqueIP.txt